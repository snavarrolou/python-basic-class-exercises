# Write an admission system for a hotel. The system will have a command
# line interface and will be controlled from a menu that looks like this:
#
# 1) Add guest to the system
# 2) Set discount for existing user
# 3) Get bill for a guest
# 4) Remove guest from the system
# 5) Exit
# Please choose an option:
# 
# - When the user chooses nr 1, the program will ask for the name of the guest
# and the number of nights. 
# - When the user chooses nr 2, the program will ask for a name and then if the guest
# exists, it will ask for a percent that will be applied to the user bill. 
# - When the user chooses nr 3, the program will ask for a name, and if the user exists, it will
# calculate the total price (at 50 euro per night) for a guest, applying discount
# if necessary.
# - When the user chooses nr 4, it will ask for a name and then remove the guest, and
# if the guest was not in the hotel, it will print a warning.
# - When the user chooses nr 5, the program exits.
#
# After the user chooses any option, the program will print the menu again and
# wait for a new command, unless the chosen option was exit.

guests = dict()

should_exit = False
while not should_exit:
    print()
    print('Menu:')
    print('1) Add guest to the system')
    print('2) Set discount for existing user')
    print('3) Get bill for a guest')
    print('4) Remove guest from the system')
    print('5) Exit')
    option = int(input('Please choose an option: '))

    if option == 1:
        guest_name = input('Please input the guest\'s name: ')
        if guest_name in guests:
            print('Guest name already exists')
            input('Press Enter to continue...')
            continue
        nights = int(input('Input the number of nights: '))
        discount = 0
        guests[guest_name] = (nights, discount)
        input('Success! Press Enter to continue...')
            
    elif option == 2:
        guest_name = input('Please input the guest\'s name: ')
        if guest_name not in guests:
            print('Guest name does not exist')
            input('Press Enter to continue...')
            continue
        discount = float(input('Input the discount percentage: '))
        nights, old_discount = guests[guest_name]
        guests[guest_name] = (nights, discount / 100)
        input('Success! Press Enter to continue...')

    elif option == 3:
        guest_name = input('Please input the guest\'s name: ')
        if guest_name not in guests:
            print('Guest name does not exist')
            input('Press Enter to continue...')
            continue
        nights, discount = guests[guest_name]
        print(f'Nights {nights} Discount {discount}')
        price = max(0, nights * 50 * (1 - discount))
        print(f'The bill for customer {guest_name} is {price:.2f} euros')
        input('Press Enter to continue...')

    elif option == 4:
        guest_name = input('Please input the guest\'s name: ')
        if guest_name not in guests:
            print('Guest name does not exist')
            input('Press Enter to continue...') 
            continue
        guests.pop(guest_name)
        print(f'Removed guest "{guest_name}" from the system')
        input('Success! Press Enter to continue...')

    elif option == 5:
        print('Thanks for using the admissions system!')
        should_exit = True
    
    else:
        print(f"Invalid option {option}")
        input('Press Enter to continue...')

