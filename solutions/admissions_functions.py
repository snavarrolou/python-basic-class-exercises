# Write an admission system for a hotel. The system will have a command
# line interface and will be controlled from a menu that looks like this:
#
# 1) Add guest to the system
# 2) Set discount for existing user
# 3) Get bill for a guest
# 4) Remove guest from the system
# 5) Exit
# Please choose an option:
# 
# - When the user chooses nr 1, the program will ask for the name of the guest
# and the number of nights. 
# - When the user chooses nr 2, the program will ask for a name and then if the guest
# exists, it will ask for a percent that will be applied to the user bill. 
# - When the user chooses nr 3, the program will ask for a name, and if the user exists, it will
# calculate the total price (at 50 euro per night) for a guest, applying discount
# if necessary.
# - When the user chooses nr 4, it will ask for a name and then remove the guest, and
# if the guest was not in the hotel, it will print a warning.
# - When the user chooses nr 5, the program exits.
#
# After the user chooses any option, the program will print the menu again and
# wait for a new command, unless the chosen option was exit.

guests = dict()


def wait_for_input():
    input('Press Enter to continue...')


def inform_success():
    print('Sucess! ', end='')
    wait_for_input()


def guest_already_exists():
    print('Guest name already exists')
    wait_for_input()


def inform_guest_does_not_exist():
    print('Guest name does not exist')
    wait_for_input()


def request_guest_name():
    return input('Please input the guest\'s name: ')


def get_menu_option():
    print()
    print('Menu:')
    print('1) Add guest to the system')
    print('2) Set discount for existing user')
    print('3) Get bill for a guest')
    print('4) Remove guest from the system')
    print('5) Exit')
    return int(input('Please choose an option: '))


def insert_guest():
    guest_name = request_guest_name()
    if guest_name in guests:
        guest_already_exists()
        return

    nights = int(input('Input the number of nights: '))
    discount = 0
    guests[guest_name] = (nights, discount)
    inform_success()


def insert_discount():
    guest_name = request_guest_name()
    if guest_name not in guests:
        guest_does_not_exist()
        return

    discount = float(input('Input the discount percentage: '))
    nights, old_discount = guests[guest_name]
    guests[guest_name] = (nights, discount / 100)
    inform_success()


def print_bill():
    guest_name = request_guest_name()
    if guest_name not in guests:
        guest_does_not_exist()
        return

    nights, discount = guests[guest_name]
    price = max(0, nights * 50 * (1 - discount))
    print(f'The bill for customer {guest_name} is {price:.2f} euros')
    wait_for_input()


def remove_guest():
    guest_name = request_guest_name()
    if guest_name not in guests:
        guest_does_not_exist()
        return

    guests.pop(guest_name)
    print(f'Removed guest "{guest_name}" from the system')
    inform_success()


should_exit = False
while not should_exit:
    option = get_menu_option()
    if option == 1:
        insert_guest()
    elif option == 2:
        insert_discount()
    elif option == 3:
        print_bill()
    elif option == 4:
        remove_guest()
    elif option == 5:
        print('Thanks for using the admissions system!')
        should_exit = True
    else:
        print(f"Invalid option {option}")
        input('Press Enter to continue...')

