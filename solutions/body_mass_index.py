# Exercise 1: Make a program that calculates the body mass index
# of the user. Ask the user what is his weight in kg and what is his
# height in meters, and then calculate the body mass index. Tell the
# user what range his BMI belongs to:
# Below 18.5 : Underweight
# 18.5 to 24.9: Normal or healthy
# 25.0 to 29.9: Slight overweight
# 30 or more: Obese
#
# If the result is overweight or more, remind the user that the BMI
# is just indicative and there are other factors that affect their
# health.
#
# The formula for the BMI is: Mass(kg) / Height(meters)^2
#
# BONUS: Give the user the option to select what units he wants to
# input weight (kg or grams) and height (meters or centimeters).

print('Welcome to the BMI program calculator!')

mass_scale = 1
mass_unit = input('What unit of mass would you like to use? (kg or g): ')
if mass_unit not in ['g', 'kg']:
    print(f'Wrong unit "{mass_unit}", exiting the program')
    exit()
elif mass_unit == 'g':
    mass_scale = 1/1000

height_scale = 1
height_unit = input('What unit of height would you like to use? (m or cm): ')
if height_unit not in ['m', 'cm']:
    print(f'Wrong unit "{height_unit}", exiting the program')
    exit()
elif height_unit == 'cm':
    height_scale = 1/100

mass = mass_scale * float(input('What is your mass?: '))
height = height_scale * float(input('What is your height?: '))

bmi = mass / (height**2)
print(f'Your BMI is {bmi:f}')

overweight_message = "Anyway, the BMI is just an indicator, there are other factors for your health"
if bmi < 18.5:
    print('You are a bit underweight')
elif bmi < 24.9:
    print('You have a healthy weight')
elif bmi < 29.9:
    print('You are a bit overweight')
    print(overweight_message)
else:
    print('You may have serious weight issues')
    print(overweight_message)