# Exercise 1: Make a program that calculates the body mass index
# of the user. Ask the user what is his weight in kg and what is his
# height in meters, and then calculate the body mass index. Tell the
# user what range his BMI belongs to:
# Below 18.5 : Underweight
# 18.5 to 24.9: Normal or healthy
# 25.0 to 29.9: Slight overweight
# 30 or more: Obese
#
# If the result is overweight or more, remind the user that the BMI
# is just indicative and there are other factors that affect their
# health.
#
# The formula for the BMI is: Mass(kg) / Height(meters)^2
#
# BONUS: Give the user the option to select what units he wants to
# input weight (kg or grams) and height (meters or centimeters).