# Write an admission system for a hotel. The system will have a command
# line interface and will be controlled from a menu that looks like this:
#
# 1) Add guest to the system
# 2) Set discount for existing user
# 3) Get bill for a guest
# 4) Remove guest from the system
# 5) Exit
# Please choose an option:
# 
# - When the user chooses nr 1, the program will ask for the name of the user
# and the number of nights. 
# - When the user chooses nr 2, the program will ask for a name and then if the guest
# exists, it will ask for a percent that will be applied to the user bill. 
# - When the user chooses nr 3, the program will ask for a name, and if the user exists, it will
# calculate the total price (at 50 euro per night) for a guest, applying discount
# if necessary.
# - When the user chooses nr 4, it will ask for a name and then remove the guest, and
# if the guest was not in the hotel, it will print a warning.
# - When the user chooses nr 5, the program exits.
#
# After the user chooses any option, the program will print the menu again and
# wait for a new command, unless the chosen option was exit.